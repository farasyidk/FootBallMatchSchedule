package rasyidk.fa.footballmatchschedule.model

import io.reactivex.Scheduler

data class ItemScheduls(val idEvent: Int,
                        val strHomeTeam: String,
                        val strAwayTeam: String,
                        val intHomeScore: String,
                        val intAwayScore: String,
                        val dateEvent: String,
                        val strHomeLineupGoalkeeper: String,
                        val strAwayLineupGoalkeeper: String,
                        val strHomeGoalDetails: String,
                        val strAwayGoalDetails: String,
                        val intHomeShots: String,
                        val intAwayShots: String,
                        val strHomeLineupDefense: String,
                        val strAwayLineupDefense: String,
                        val strHomeLineupMidfield: String,
                        val strAwayLineupMidfield: String,
                        val strHomeLineupForward: String,
                        val strAwayLineupForward: String,
                        val strHomeLineupSubstitutes: String,
                        val strAwayLineupSubstitutes: String,
                        val strHomeFormation: String,
                        val strAwayFormation: String,
                        val strTeamBadge: String,
                        val idHomeTeam: String,
                        val idAwayTeam: String)