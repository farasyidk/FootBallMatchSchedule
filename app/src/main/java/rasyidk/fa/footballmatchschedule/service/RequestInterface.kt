package rasyidk.fa.footballmatchschedule.service

import io.reactivex.Observable
import rasyidk.fa.footballmatchschedule.BuildConfig
import rasyidk.fa.footballmatchschedule.model.ItemScheduls
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RequestInterface {
    @GET("api/v1/json/${BuildConfig.TSDB_API_KEY}/eventspastleague.php?id={idPastLeague}")
    fun getPastMatchEvent(@Path("idPastLeague")idPassLeague: String): Observable<ScheduleResponse>

    @GET("api/v1/json/${BuildConfig.TSDB_API_KEY}/eventsnextleague.php?id={idPastLeague}")
    fun getNextMatchEvent(@Path("idPassLeague")idPassLeague: String): Observable<ScheduleResponse>

    @GET("api/v1/json/1/lookupevent.php")
    fun getDetailMatchEvent(@Query("id")idPassLeague: String): Observable<ScheduleResponse>

//    @GET("/users?filters[0][operator]=equals")
//    fun retrieveUsersByFilters(
//            @Query("filters[0][field]") nameFilter: String,
//            @Query("filters[0][value]") value: String): UserDto
}